//
//  SimpleDesignCVC.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 29/09/23.
//

import UIKit

@available(iOS 13.0, *)
class SimpleDesignCVC: UICollectionViewCell {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgBG: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     var isLiked = false // Initial state is not liked
    
    // Handle the "like" button tap
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        isLiked.toggle() // Toggle the liked state
        updateLikeButtonUI()
    }
    
    func updateLikeButtonUI() {
        if isLiked {
            btnLike.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            btnLike.tintColor = .red
        } else {
            btnLike.setImage(UIImage(systemName: "heart"), for: .normal)
            btnLike.tintColor = UIColor(named: "title")
        }
    }
    
    var shareActionHandler: (() -> Void)?
       
    @IBAction func shareButtonClicked(_ sender: UIButton) {
        shareActionHandler?()
    }
}
