//
//  HomePageVC.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 29/09/23.
//

import UIKit

class HomePageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBAction func btnSimpleDesign(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SimpleDesignVC") as! SimpleDesignVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnArtwork(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ArtWorkVC") as! ArtWorkVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnAnime(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AnimeVC") as! AnimeVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnMemes(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MemesVC") as! MemesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBestAI(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BestSiteVC") as! BestSiteVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnAbout(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

