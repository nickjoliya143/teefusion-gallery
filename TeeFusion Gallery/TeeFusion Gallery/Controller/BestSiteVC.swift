//
//  BestSiteVC.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 29/09/23.
//

import UIKit

class BestSiteVC: UIViewController {

    @IBOutlet weak var CVSimpleDesign: UICollectionView!
    var tshirtDesigns = [TshirtDesign]()
    override func viewDidLoad() {
        super.viewDidLoad()

        CVSimpleDesign.delegate = self
        CVSimpleDesign.dataSource = self
        
        tshirtDesigns = TshirtDataManager.shared.getSitesForTshirtDesigns()
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BestSiteVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tshirtDesigns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSiteCVC", for: indexPath) as! BestSiteCVC
        
        cell.lblTitle.text = tshirtDesigns[indexPath.row].name
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width  , height: 100)
    }
}

