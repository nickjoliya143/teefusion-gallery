//
//  MemesVC.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 29/09/23.
//

import UIKit

class MemesVC: UIViewController {

    @IBOutlet weak var CVSimpleDesign: UICollectionView!
    var tshirtDesigns = [TshirtDesign]()
    override func viewDidLoad() {
        super.viewDidLoad()

        CVSimpleDesign.delegate = self
        CVSimpleDesign.dataSource = self
        
        tshirtDesigns = TshirtDataManager.shared.getMemesTshirtDesigns()
        CVSimpleDesign.register(UINib(nibName: "SimpleDesignCVC", bundle: nil), forCellWithReuseIdentifier: "SimpleDesignCVC")
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

@available(iOS 13.0, *)
extension MemesVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tshirtDesigns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimpleDesignCVC", for: indexPath) as! SimpleDesignCVC
        
        cell.imgBG.image = tshirtDesigns[indexPath.row].image
        cell.updateLikeButtonUI()
        cell.btnSave.tag = indexPath.item
        cell.btnSave.addTarget(self, action: #selector(saveButtonClicked(_:)), for: .touchUpInside)
        cell.shareActionHandler = { [weak self] in
             self?.shareItem(at: indexPath)
        }
         

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width / 2 , height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    @objc func saveButtonClicked(_ sender: UIButton) {
        // Retrieve the cell at the clicked index path
        let indexPath = IndexPath(item: sender.tag, section: 0)
        
        // Replace 'collectionView' with your actual UICollectionView instance
        if let cell = CVSimpleDesign.cellForItem(at: indexPath) as? SimpleDesignCVC {
            // Capture the cell's content as an image
            let capturedImage = cell.contentView.asImage()
            
            // Save the captured image to a file
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let fileName = "capturedImage_\(indexPath.item).png"
                let fileURL = documentsDirectory.appendingPathComponent(fileName)
                
                if let imageData = capturedImage.pngData() {
                    do {
                        try imageData.write(to: fileURL)
                        print("Image saved to \(fileURL.path)")
                        // Create an alert controller
                               let alertController = UIAlertController(title: "Thanks", message: "Your Design Saved Successfully.", preferredStyle: .alert)

                               // Create an "OK" button action
                               let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                   // Handle the "OK" button action
                                   print("OK button tapped")
                               }

                               // Add the "OK" button action to the alert controller
                               alertController.addAction(okAction)

                               // Present the alert controller
                               self.present(alertController, animated: true, completion: nil)
                    } catch {
                        print("Error saving image: \(error)")
                    }
                }
            }
        }
    }
    func shareItem(at indexPath: IndexPath) {
        // Create an array of items to share (e.g., text, URLs, images, etc.)
        let items: [Any] = ["Share Your Design"]

        // Initialize the activity view controller
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)

        // Set a completion handler for when the sharing is completed (optional)
        activityViewController.completionWithItemsHandler = { [weak self] activityType, completed, returnedItems, error in
            if completed {
                // Sharing was successful
                print("Shared successfully via \(activityType)")
            } else {
                // Sharing was canceled or failed
                print("Sharing canceled or failed")
            }
        }

        // Present the activity view controller
        if let presenter = self.presentedViewController {
            presenter.present(activityViewController, animated: true, completion: nil)
        } else {
            present(activityViewController, animated: true, completion: nil)
        }
    }


}
