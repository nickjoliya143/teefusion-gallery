//
//  TshirtDataManager.swift
//  TeeFusion Gallery
//
//  Created by Nick Joliya on 29/09/23.
//

import Foundation
import UIKit


struct TshirtDesign {
    let id: Int
    let name: String
    let image: UIImage?
    
}

class TshirtDataManager {
    static let shared = TshirtDataManager()
    
    private init() {
        // Private initializer to enforce singleton pattern
    }
    
    func getDummyTshirtDesigns() -> [TshirtDesign] {
        var designs = [TshirtDesign]()
        
        let design1 = TshirtDesign(id: 1, name: "Design 1", image: UIImage(named: "s1"))
        let design2 = TshirtDesign(id: 2, name: "Design 2", image: UIImage(named: "s2"))
        let design3 = TshirtDesign(id: 3, name: "Design 3", image: UIImage(named: "s3"))
        let design4 = TshirtDesign(id: 4, name: "Design 4", image: UIImage(named: "s4"))
        let design5 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "s5"))
        let design6 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "s6"))
   
        
        designs.append(contentsOf: [design1, design2, design3, design4 , design5 , design6])
        
        return designs
    }
    func getArtTshirtDesigns() -> [TshirtDesign] {
        var designs = [TshirtDesign]()
        
        let design1 = TshirtDesign(id: 1, name: "Design 1", image: UIImage(named: "a1"))
        let design2 = TshirtDesign(id: 2, name: "Design 2", image: UIImage(named: "a2"))
        let design3 = TshirtDesign(id: 3, name: "Design 3", image: UIImage(named: "a3"))
        let design4 = TshirtDesign(id: 4, name: "Design 4", image: UIImage(named: "a4"))
        let design5 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "a5"))
        let design6 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "a6"))
        let design7 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "a7"))
        let design8 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "a8"))
   
        
        designs.append(contentsOf: [design1, design2, design3, design4 , design5 , design6 ,design7 ,design8])
        
        return designs
    }
    
    func getAnimeTshirtDesigns() -> [TshirtDesign] {
        var designs = [TshirtDesign]()
        
        let design1 = TshirtDesign(id: 1, name: "Design 1", image: UIImage(named: "e1"))
        let design2 = TshirtDesign(id: 2, name: "Design 2", image: UIImage(named: "e2"))
        let design3 = TshirtDesign(id: 3, name: "Design 3", image: UIImage(named: "e3"))
        let design4 = TshirtDesign(id: 4, name: "Design 4", image: UIImage(named: "e4"))
        let design5 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "e5"))
        let design6 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "e6"))
        let design7 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "e7"))
        let design8 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "e8"))
   
        
        designs.append(contentsOf: [design1, design2, design3, design4 , design5 , design6 ,design7 ,design8])
        
        return designs
    }
    func getMemesTshirtDesigns() -> [TshirtDesign] {
        var designs = [TshirtDesign]()
        
        let design1 = TshirtDesign(id: 1, name: "Design 1", image: UIImage(named: "m1"))
        let design2 = TshirtDesign(id: 2, name: "Design 2", image: UIImage(named: "m2"))
        let design3 = TshirtDesign(id: 3, name: "Design 3", image: UIImage(named: "m3"))
        let design4 = TshirtDesign(id: 4, name: "Design 4", image: UIImage(named: "m4"))
        let design5 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "m5"))
        let design6 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "m6"))
        let design7 = TshirtDesign(id: 5, name: "Design 5", image: UIImage(named: "m7"))
        let design8 = TshirtDesign(id: 6, name: "Design 6", image: UIImage(named: "m8"))
   
        
        designs.append(contentsOf: [design1, design2, design3, design4 , design5 , design6 ,design7 ,design8])
        
        return designs
    }
    func getSitesForTshirtDesigns() -> [TshirtDesign] {
        var designs = [TshirtDesign]()
        
        let design1 = TshirtDesign(id: 1, name: "Teespring", image: UIImage(named: "m1"))
        let design2 = TshirtDesign(id: 2, name: "Zazzle", image: UIImage(named: "m2"))
        let design3 = TshirtDesign(id: 3, name: "Cafepress", image: UIImage(named: "m3"))
        let design4 = TshirtDesign(id: 4, name: "Redbubble", image: UIImage(named: "m4"))
        let design5 = TshirtDesign(id: 5, name: "Merch by Amazon", image: UIImage(named: "m5"))
        let design6 = TshirtDesign(id: 6, name: "Society6", image: UIImage(named: "m6"))
        let design7 = TshirtDesign(id: 5, name: "Spreadshirt", image: UIImage(named: "m7"))
        let design8 = TshirtDesign(id: 6, name: "Threadless", image: UIImage(named: "m8"))
   
        
        designs.append(contentsOf: [design1, design2, design3, design4 , design5 , design6 ,design7 ,design8])
        
        return designs
    }
}
